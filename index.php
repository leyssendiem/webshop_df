<?php
include_once __DIR__ . '/autoload.php';
include 'functions.php';

if (loggedin()) {
    header('location:home.php');
}

$email = '';
$expire = time() + 60 * 60 * 24 * 30;

$pass = '';
$expire = time() + 60 * 60 * 24 * 30;

$rememberme = '';
$expire = time() + 60 * 60 * 24 * 30;


if (isset($_POST['login'])) {


    if (isset($_POST['remember-me'])) {

        $email = $_POST['email'];
        $pass = $_POST['password'];
        $rememberme = $_POST['remember-me'];
        setcookie('user', $email, $expire);
        setcookie('password', $pass, $expire);
        setcookie('rememberme', $rememberme, $expire);

        if (isset($_COOKIE['user'])) {
            $email = $_COOKIE['user'];

        }
        if (isset($_COOKIE['password'])) {
            $pass = $_COOKIE['password'];

        }
        if (isset($_COOKIE['rememberme'])) {
            $rememberme = $_COOKIE['rememberme'];

        }
    } else {
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;

        if (isset($_SESSION['username'])) {
            $email = $_SESSION['username'];

        }

        if (isset($_SESSION['password'])) {
            $pass = $_SESSION['password'];

        }
    }


    header('location:home.php');
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sign in &middot; Twitter Bootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-image: url(assets/img/Pattern.jpeg);
            color: #fff;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #666;
            border: 1px solid #666;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
            color: #fff;
        }

        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

        #login {
            -webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            -moz-box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
        }

        #register {
            -webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            -moz-box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
        }

        #wachtwoord {
            margin-top: 10px;
            -webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            -moz-box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
        }

    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
</head>

<body>

<div class="container">

    <form action="index.php" method="post" class="form-signin">
        <h2 class="form-signin-heading">Log in</h2>
        <input type="text" class="input-block-level" placeholder="Emailadres" name="email">
        <input type="password" class="input-block-level" placeholder="Wachtwoord" name="password">
        <label class="checkbox">
            <input type="checkbox" value="remember-me" name="remember-me"> Onthoud mij
        </label>
        <button class="btn btn-large btn-primary" type="submit" id="login" name="login">Log in</button>
        <br/>
        <button class="btn btn-small btn-primary" id="wachtwoord" type="button">Wachtwoord vergeten?</button>
    </form>

</div>
<!-- /container -->
<div class="form-signin">
    <h2 class="form-signin-heading">Nog geen account?</h2>
    <a href="registration.php" class="btn btn-large btn-primary" id="register">Registreer</a>
</div>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap-transition.js"></script>
<script src="assets/js/bootstrap-alert.js"></script>
<script src="assets/js/bootstrap-modal.js"></script>
<script src="assets/js/bootstrap-dropdown.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>
<script src="assets/js/bootstrap-tab.js"></script>
<script src="assets/js/bootstrap-tooltip.js"></script>
<script src="assets/js/bootstrap-popover.js"></script>
<script src="assets/js/bootstrap-button.js"></script>
<script src="assets/js/bootstrap-collapse.js"></script>
<script src="assets/js/bootstrap-carousel.js"></script>
<script src="assets/js/bootstrap-typeahead.js"></script>

</body>
</html>
