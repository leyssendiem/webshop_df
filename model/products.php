<?php
// commented code = prioriteit 2 of 1

class products {
    protected $aanmaken;
    protected $verwijderen;
    protected $wijzigen;
    protected $CatAdd;
    protected $CatDel;
    protected $AddToCat;
    protected $RemoveFromCat;
    protected $AddPromote;
    protected $RemovePromote;
    protected $PublicPage;
    protected $SearchProducts;
    //protected $RelatedProducts;
    //protected $CompareProducts;
    protected $MarkedProducts;

    public function setAddPromote($AddPromote)
    {
        $this->AddPromote = $AddPromote;
    }

    public function getAddPromote()
    {
        return $this->AddPromote;
    }

    public function setAddToCat($AddToCat)
    {
        $this->AddToCat = $AddToCat;
    }

    public function getAddToCat()
    {
        return $this->AddToCat;
    }

    public function setCatAdd($CatAdd)
    {
        $this->CatAdd = $CatAdd;
    }

    public function getCatAdd()
    {
        return $this->CatAdd;
    }

    public function setCatDel($CatDel)
    {
        $this->CatDel = $CatDel;
    }

    public function getCatDel()
    {
        return $this->CatDel;
    }

    public function setMarkedProducts($MarkedProducts)
    {
        $this->MarkedProducts = $MarkedProducts;
    }

    public function getMarkedProducts()
    {
        return $this->MarkedProducts;
    }

    public function setPublicPage($PublicPage)
    {
        $this->PublicPage = $PublicPage;
    }

    public function getPublicPage()
    {
        return $this->PublicPage;
    }

    public function setRemoveFromCat($RemoveFromCat)
    {
        $this->RemoveFromCat = $RemoveFromCat;
    }

    public function getRemoveFromCat()
    {
        return $this->RemoveFromCat;
    }

    public function setRemovePromote($RemovePromote)
    {
        $this->RemovePromote = $RemovePromote;
    }

    public function getRemovePromote()
    {
        return $this->RemovePromote;
    }

    public function setSearchProducts($SearchProducts)
    {
        $this->SearchProducts = $SearchProducts;
    }

    public function getSearchProducts()
    {
        return $this->SearchProducts;
    }

    public function setAanmaken($aanmaken)
    {
        $this->aanmaken = $aanmaken;
    }

    public function getAanmaken()
    {
        return $this->aanmaken;
    }

    public function setVerwijderen($verwijderen)
    {
        $this->verwijderen = $verwijderen;
    }

    public function getVerwijderen()
    {
        return $this->verwijderen;
    }

    public function setWijzigen($wijzigen)
    {
        $this->wijzigen = $wijzigen;
    }

    public function getWijzigen()
    {
        return $this->wijzigen;
    }

}