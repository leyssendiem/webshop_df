<?php

class login {
    protected $email;
    protected $password;
    protected $rememberMe;

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setRememberMe($rememberMe)
    {
        $this->rememberMe = $rememberMe;
    }

    public function getRememberMe()
    {
        return $this->rememberMe;
    }

}