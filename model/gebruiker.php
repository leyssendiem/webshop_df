<?php
class User
{
    private $naam = '';
    private $voornaam = '';
    private $gebruikersNaam = '';
    private $wachtwoord = '';
    private $adres = '';
    private $telefoon = '';
    private $email = '';
    private $facturatieAdres = '';
    private $leverAdres = '';
    private $voorkeurBetalingsSysteem = '';
    private $interesseInNieuwsbrief = '';
    private $heeftRegistratieDoorlopen = '';
    private $winkelwagen = '';
    private $bestellingen = '';
    private $favorieten = '';
    private $isGeblokkeerd = '';

    public function __constructor()
    {

    }

    public function setAdres($adres)
    {
        $this->adres = $adres;
    }

    public function getAdres()
    {
        return $this->adres;
    }

    public function setBestellingen($bestellingen)
    {
        $this->bestellingen = $bestellingen;
    }

    public function getBestellingen()
    {
        return $this->bestellingen;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setFacturatieAdres($facturatieAdres)
    {
        $this->facturatieAdres = $facturatieAdres;
    }

    public function getFacturatieAdres()
    {
        return $this->facturatieAdres;
    }

    public function setFavorieten($favorieten)
    {
        $this->favorieten = $favorieten;
    }

    public function getFavorieten()
    {
        return $this->favorieten;
    }

    public function setGebruikersNaam($gebruikersNaam)
    {
        $this->gebruikersNaam = $gebruikersNaam;
    }

    public function getGebruikersNaam()
    {
        return $this->gebruikersNaam;
    }

    public function setHeeftRegistratieDoorlopen($heeftRegistratieDoorlopen)
    {
        $this->heeftRegistratieDoorlopen = $heeftRegistratieDoorlopen;
    }

    public function getHeeftRegistratieDoorlopen()
    {
        return $this->heeftRegistratieDoorlopen;
    }

    public function setInteresseInNieuwsbrief($interesseInNieuwsbrief)
    {
        $this->interesseInNieuwsbrief = $interesseInNieuwsbrief;
    }

    public function getInteresseInNieuwsbrief()
    {
        return $this->interesseInNieuwsbrief;
    }

    public function setIsGeblokkeerd($isGeblokkeerd)
    {
        $this->isGeblokkeerd = $isGeblokkeerd;
    }

    public function getIsGeblokkeerd()
    {
        return $this->isGeblokkeerd;
    }

    public function setLeverAdres($leverAdres)
    {
        $this->leverAdres = $leverAdres;
    }

    public function getLeverAdres()
    {
        return $this->leverAdres;
    }

    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    public function getNaam()
    {
        return $this->naam;
    }

    public function setTelefoon($telefoon)
    {
        $this->telefoon = $telefoon;
    }

    public function getTelefoon()
    {
        return $this->telefoon;
    }

    public function setVoorkeurBetalingsSysteem($voorkeurBetalingsSysteem)
    {
        $this->voorkeurBetalingsSysteem = $voorkeurBetalingsSysteem;
    }

    public function getVoorkeurBetalingsSysteem()
    {
        return $this->voorkeurBetalingsSysteem;
    }

    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;
    }

    public function getVoornaam()
    {
        return $this->voornaam;
    }

    public function setWachtwoord($wachtwoord)
    {
        $this->wachtwoord = $wachtwoord;
    }

    public function getWachtwoord()
    {
        return $this->wachtwoord;
    }

    public function setWinkelwagen($winkelwagen)
    {
        $this->winkelwagen = $winkelwagen;
    }

    public function getWinkelwagen()
    {
        return $this->winkelwagen;
    }


    public function registration()
    {

    }


    public function login()
    {

    }


    public function logout()
    {

    }

    public function validate()
    {

    }

}
?>

