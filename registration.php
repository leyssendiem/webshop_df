<?php
include_once __DIR__ . '/utilities/validation.php';
include_once __DIR__ . '/utilities/formHelpers.php';
include_once __DIR__ . '/autoload.php';
use model\Form\registration;

$validationResults = array();
$formHasErrors = false;

if (isset($_POST['register'])) {
    // De gebruiker heeft de login button geklikt.
    // Valideer de gegevens
    $validationResults['name'] = validateString($_POST['name'], 'your name');
    $validationResults['lastname'] = validateString($_POST['lastname'], 'your last name');
    $validationResults['email'] = validateEmail($_POST['email']);
    $validationResults['password'] = validatePassword($_POST['password']);
    $validationResults['confirmed-password'] = validatePassword($_POST['confirmed-password'], true);

    // Het paswoord veld en het bevestigt paswoord horen hetzelfde te zijn.
    if ($_POST['password'] !== $_POST['confirmed-password']) {
        $validationResults['error']['confirmed-password'] = 'De wachtwoorden komen niet overeen';
    }

    $validationResults['street'] = validateString($_POST['street'], 'your street address');
    $validationResults['zip'] = validateString($_POST['zip'], 'your zip code');
    $validationResults['city'] = validateString($_POST['city'], 'your city');
    $validationResults['country'] = validateString($_POST['country'], 'your country');
    $validationResults['phone'] = validateString($_POST['phone'], 'your phone number');
    $validationResults['deliver'] = validateString($_POST['deliver'], 'your deliver address');
    $validationResults['payment'] = validateString($_POST['payment'], 'your paymentmethod');

    foreach ($validationResults as $result) {
        if (isset($result['error'])) {
            $formHasErrors = true;
            break;
        }
    }

    if ($formHasErrors === false) {
        // Sla de waarden op in de database.
        header('Location: home.php');
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bootstrap, from Twitter</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-image:url(assets/img/Pattern.jpeg);
                color:#fff;
            }
            #wrapper{
                margin:auto;
                width:960px;
            }

            .registration {
                max-width: 350px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #666;
                border: 1px solid #666;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .registration .registration-heading {
                margin-bottom: 10px;
            }
            .registration input[type="text"],
            .registration input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

            .registration label.error {
                color: red;
            }

            #buttons{
                margin-left:50px;
            }
        </style>
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="assets/ico/favicon.png">
    </head>

    <body>

        <div id="wrapper">
            <div class="container1">

                <form class="registration" action="registration.php" method="post">
                    <h2 class="registration-heading">Meld u aan</h2>
                    <h3>Belangrijke informatie</h3>

                    <?php echo getErrorLabel($validationResults, 'name'); ?>
                    <input type="text" class="input-block-level" name="name" id="email" placeholder="Voornaam" value="<?php echo getValue($validationResults, 'name'); ?>">
                    
                    <?php echo getErrorLabel($validationResults, 'lastname'); ?>
                    <input type="text" class="input-block-level" name="lastname" id="email" placeholder="Naam" value="<?php echo getValue($validationResults, 'lastname'); ?>">
                    
                    <?php echo getErrorLabel($validationResults, 'email'); ?>
                    <input type="text" class="input-block-level" name="email" id="email" placeholder="Emailadres" value="<?php echo getValue($validationResults, 'email'); ?>">

                    <?php echo getErrorLabel($validationResults, 'password'); ?>
                    <input type="password" class="input-block-level" name="password" id="password" placeholder="Wachtwoord">

                    <?php echo getErrorLabel($validationResults, 'confirmed-password'); ?>
                    <input type="password" class="input-block-level" name="confirmed-password" id="confirmed-password" placeholder="Bevestig uw wachtwoord">

                    <?php echo getErrorLabel($validationResults, 'street'); ?>
                    <input type="text" class="input-block-level" name="street" id="street" placeholder="Adres" value="<?php echo getValue($validationResults, 'street'); ?>">

                    <?php echo getErrorLabel($validationResults, 'zip'); ?>
                    <input type="text" class="input-block-level" name="zip" id="zip" placeholder="Postcode" value="<?php echo getValue($validationResults, 'zip'); ?>">

                    <?php echo getErrorLabel($validationResults, 'city'); ?>
                    <input type="text" class="input-block-level" name="city" id="city" placeholder="Stad" value="<?php echo getValue($validationResults, 'city'); ?>">

<?php echo getErrorLabel($validationResults, 'country'); ?>
                    <select class="input-block-level" name="country" id="country" placeholder="Land" value="<?php echo getValue($validationResults, 'country'); ?>"
                            style="margin-bottom:15px;">
                        <option value="Belgium" title="Belgium">Belgium</option>
                        <option value="France" title="France">France</option>
                        <option value="Germany" title="Germany">Germany</option>
                        <option value="Netherlands" title="Netherlands">Netherlands</option>
                        <option disabled>-------------------------------</option>
                        <option value="Afghanistan" title="Afghanistan">Afghanistan</option>
                        <option value="Åland Islands" title="Åland Islands">Åland Islands</option>
                        <option value="Albania" title="Albania">Albania</option>
                        <option value="Algeria" title="Algeria">Algeria</option>
                        <option value="American Samoa" title="American Samoa">American Samoa</option>
                        <option value="Andorra" title="Andorra">Andorra</option>
                        <option value="Angola" title="Angola">Angola</option>
                        <option value="Anguilla" title="Anguilla">Anguilla</option>
                        <option value="Antarctica" title="Antarctica">Antarctica</option>
                        <option value="Antigua and Barbuda" title="Antigua and Barbuda">Antigua and Barbuda</option>
                        <option value="Argentina" title="Argentina">Argentina</option>
                        <option value="Armenia" title="Armenia">Armenia</option>
                        <option value="Aruba" title="Aruba">Aruba</option>
                        <option value="Australia" title="Australia">Australia</option>
                        <option value="Austria" title="Austria">Austria</option>
                        <option value="Azerbaijan" title="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas" title="Bahamas">Bahamas</option>
                        <option value="Bahrain" title="Bahrain">Bahrain</option>
                        <option value="Bangladesh" title="Bangladesh">Bangladesh</option>
                        <option value="Barbados" title="Barbados">Barbados</option>
                        <option value="Belarus" title="Belarus">Belarus</option>                   
                        <option value="Belize" title="Belize">Belize</option>
                        <option value="Benin" title="Benin">Benin</option>
                        <option value="Bermuda" title="Bermuda">Bermuda</option>
                        <option value="Bhutan" title="Bhutan">Bhutan</option>
                        <option value="Bolivia, Plurinational State of" title="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option>
                        <option value="Bonaire, Sint Eustatius and Saba" title="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                        <option value="Bosnia and Herzegovina" title="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                        <option value="Botswana" title="Botswana">Botswana</option>
                        <option value="Bouvet Island" title="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil" title="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory" title="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="Brunei Darussalam" title="Brunei Darussalam">Brunei Darussalam</option>
                        <option value="Bulgaria" title="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso" title="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi" title="Burundi">Burundi</option>
                        <option value="Cambodia" title="Cambodia">Cambodia</option>
                        <option value="Cameroon" title="Cameroon">Cameroon</option>
                        <option value="Canada" title="Canada">Canada</option>
                        <option value="Cape Verde" title="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands" title="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic" title="Central African Republic">Central African Republic</option>
                        <option value="Chad" title="Chad">Chad</option>
                        <option value="Chile" title="Chile">Chile</option>
                        <option value="China" title="China">China</option>
                        <option value="Christmas Island" title="Christmas Island">Christmas Island</option>
                        <option value="Cocos (Keeling) Islands" title="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia" title="Colombia">Colombia</option>
                        <option value="Comoros" title="Comoros">Comoros</option>
                        <option value="Congo" title="Congo">Congo</option>
                        <option value="Congo, the Democratic Republic of the" title="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                        <option value="Cook Islands" title="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica" title="Costa Rica">Costa Rica</option>
                        <option value="Côte d'Ivoire" title="Côte d'Ivoire">Côte d'Ivoire</option>
                        <option value="Croatia" title="Croatia">Croatia</option>
                        <option value="Cuba" title="Cuba">Cuba</option>
                        <option value="Curaçao" title="Curaçao">Curaçao</option>
                        <option value="Cyprus" title="Cyprus">Cyprus</option>
                        <option value="Czech Republic" title="Czech Republic">Czech Republic</option>
                        <option value="Denmark" title="Denmark">Denmark</option>
                        <option value="Djibouti" title="Djibouti">Djibouti</option>
                        <option value="Dominica" title="Dominica">Dominica</option>
                        <option value="Dominican Republic" title="Dominican Republic">Dominican Republic</option>
                        <option value="Ecuador" title="Ecuador">Ecuador</option>
                        <option value="Egypt" title="Egypt">Egypt</option>
                        <option value="El Salvador" title="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea" title="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea" title="Eritrea">Eritrea</option>
                        <option value="Estonia" title="Estonia">Estonia</option>
                        <option value="Ethiopia" title="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands (Malvinas)" title="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                        <option value="Faroe Islands" title="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji" title="Fiji">Fiji</option>
                        <option value="Finland" title="Finland">Finland</option>
                        <option value="French Guiana" title="French Guiana">French Guiana</option>
                        <option value="French Polynesia" title="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories" title="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon" title="Gabon">Gabon</option>
                        <option value="Gambia" title="Gambia">Gambia</option>
                        <option value="Georgia" title="Georgia">Georgia</option>
                        <option value="Ghana" title="Ghana">Ghana</option>
                        <option value="Gibraltar" title="Gibraltar">Gibraltar</option>
                        <option value="Greece" title="Greece">Greece</option>
                        <option value="Greenland" title="Greenland">Greenland</option>
                        <option value="Grenada" title="Grenada">Grenada</option>
                        <option value="Guadeloupe" title="Guadeloupe">Guadeloupe</option>
                        <option value="Guam" title="Guam">Guam</option>
                        <option value="Guatemala" title="Guatemala">Guatemala</option>
                        <option value="Guernsey" title="Guernsey">Guernsey</option>
                        <option value="Guinea" title="Guinea">Guinea</option>
                        <option value="Guinea-Bissau" title="Guinea-Bissau">Guinea-Bissau</option>
                        <option value="Guyana" title="Guyana">Guyana</option>
                        <option value="Haiti" title="Haiti">Haiti</option>
                        <option value="Heard Island and McDonald Islands" title="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                        <option value="Holy See (Vatican City State)" title="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                        <option value="Honduras" title="Honduras">Honduras</option>
                        <option value="Hong Kong" title="Hong Kong">Hong Kong</option>
                        <option value="Hungary" title="Hungary">Hungary</option>
                        <option value="Iceland" title="Iceland">Iceland</option>
                        <option value="India" title="India">India</option>
                        <option value="Indonesia" title="Indonesia">Indonesia</option>
                        <option value="Iran, Islamic Republic of" title="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                        <option value="Iraq" title="Iraq">Iraq</option>
                        <option value="Ireland" title="Ireland">Ireland</option>
                        <option value="Isle of Man" title="Isle of Man">Isle of Man</option>
                        <option value="Israel" title="Israel">Israel</option>
                        <option value="Italy" title="Italy">Italy</option>
                        <option value="Jamaica" title="Jamaica">Jamaica</option>
                        <option value="Japan" title="Japan">Japan</option>
                        <option value="Jersey" title="Jersey">Jersey</option>
                        <option value="Jordan" title="Jordan">Jordan</option>
                        <option value="Kazakhstan" title="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya" title="Kenya">Kenya</option>
                        <option value="Kiribati" title="Kiribati">Kiribati</option>
                        <option value="Korea, Democratic People's Republic of" title="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                        <option value="Korea, Republic of" title="Korea, Republic of">Korea, Republic of</option>
                        <option value="Kuwait" title="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan" title="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Lao People's Democratic Republic" title="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                        <option value="Latvia" title="Latvia">Latvia</option>
                        <option value="Lebanon" title="Lebanon">Lebanon</option>
                        <option value="Lesotho" title="Lesotho">Lesotho</option>
                        <option value="Liberia" title="Liberia">Liberia</option>
                        <option value="Libya" title="Libya">Libya</option>
                        <option value="Liechtenstein" title="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania" title="Lithuania">Lithuania</option>
                        <option value="Luxembourg" title="Luxembourg">Luxembourg</option>
                        <option value="Macao" title="Macao">Macao</option>
                        <option value="Macedonia, the former Yugoslav Republic of" title="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option>
                        <option value="Madagascar" title="Madagascar">Madagascar</option>
                        <option value="Malawi" title="Malawi">Malawi</option>
                        <option value="Malaysia" title="Malaysia">Malaysia</option>
                        <option value="Maldives" title="Maldives">Maldives</option>
                        <option value="Mali" title="Mali">Mali</option>
                        <option value="Malta" title="Malta">Malta</option>
                        <option value="Marshall Islands" title="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique" title="Martinique">Martinique</option>
                        <option value="Mauritania" title="Mauritania">Mauritania</option>
                        <option value="Mauritius" title="Mauritius">Mauritius</option>
                        <option value="Mayotte" title="Mayotte">Mayotte</option>
                        <option value="Mexico" title="Mexico">Mexico</option>
                        <option value="Micronesia, Federated States of" title="Micronesia, Federated States of">Micronesia, Federated States of</option>
                        <option value="Moldova, Republic of" title="Moldova, Republic of">Moldova, Republic of</option>
                        <option value="Monaco" title="Monaco">Monaco</option>
                        <option value="Mongolia" title="Mongolia">Mongolia</option>
                        <option value="Montenegro" title="Montenegro">Montenegro</option>
                        <option value="Montserrat" title="Montserrat">Montserrat</option>
                        <option value="Morocco" title="Morocco">Morocco</option>
                        <option value="Mozambique" title="Mozambique">Mozambique</option>
                        <option value="Myanmar" title="Myanmar">Myanmar</option>
                        <option value="Namibia" title="Namibia">Namibia</option>
                        <option value="Nauru" title="Nauru">Nauru</option>
                        <option value="Nepal" title="Nepal">Nepal</option>
                        <option value="New Caledonia" title="New Caledonia">New Caledonia</option>
                        <option value="New Zealand" title="New Zealand">New Zealand</option>
                        <option value="Nicaragua" title="Nicaragua">Nicaragua</option>
                        <option value="Niger" title="Niger">Niger</option>
                        <option value="Nigeria" title="Nigeria">Nigeria</option>
                        <option value="Niue" title="Niue">Niue</option>
                        <option value="Norfolk Island" title="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands" title="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="Norway" title="Norway">Norway</option>
                        <option value="Oman" title="Oman">Oman</option>
                        <option value="Pakistan" title="Pakistan">Pakistan</option>
                        <option value="Palau" title="Palau">Palau</option>
                        <option value="Palestinian Territory, Occupied" title="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                        <option value="Panama" title="Panama">Panama</option>
                        <option value="Papua New Guinea" title="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay" title="Paraguay">Paraguay</option>
                        <option value="Peru" title="Peru">Peru</option>
                        <option value="Philippines" title="Philippines">Philippines</option>
                        <option value="Pitcairn" title="Pitcairn">Pitcairn</option>
                        <option value="Poland" title="Poland">Poland</option>
                        <option value="Portugal" title="Portugal">Portugal</option>
                        <option value="Puerto Rico" title="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar" title="Qatar">Qatar</option>
                        <option value="Réunion" title="Réunion">Réunion</option>
                        <option value="Romania" title="Romania">Romania</option>
                        <option value="Russian Federation" title="Russian Federation">Russian Federation</option>
                        <option value="Rwanda" title="Rwanda">Rwanda</option>
                        <option value="Saint Barthélemy" title="Saint Barthélemy">Saint Barthélemy</option>
                        <option value="Saint Helena, Ascension and Tristan da Cunha" title="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
                        <option value="Saint Kitts and Nevis" title="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option value="Saint Lucia" title="Saint Lucia">Saint Lucia</option>
                        <option value="Saint Martin (French part)" title="Saint Martin (French part)">Saint Martin (French part)</option>
                        <option value="Saint Pierre and Miquelon" title="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                        <option value="Saint Vincent and the Grenadines" title="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                        <option value="Samoa" title="Samoa">Samoa</option>
                        <option value="San Marino" title="San Marino">San Marino</option>
                        <option value="Sao Tome and Principe" title="Sao Tome and Principe">Sao Tome and Principe</option>
                        <option value="Saudi Arabia" title="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal" title="Senegal">Senegal</option>
                        <option value="Serbia" title="Serbia">Serbia</option>
                        <option value="Seychelles" title="Seychelles">Seychelles</option>
                        <option value="Sierra Leone" title="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore" title="Singapore">Singapore</option>
                        <option value="Sint Maarten (Dutch part)" title="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
                        <option value="Slovakia" title="Slovakia">Slovakia</option>
                        <option value="Slovenia" title="Slovenia">Slovenia</option>
                        <option value="Solomon Islands" title="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia" title="Somalia">Somalia</option>
                        <option value="South Africa" title="South Africa">South Africa</option>
                        <option value="South Georgia and the South Sandwich Islands" title="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                        <option value="South Sudan" title="South Sudan">South Sudan</option>
                        <option value="Spain" title="Spain">Spain</option>
                        <option value="Sri Lanka" title="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan" title="Sudan">Sudan</option>
                        <option value="Suriname" title="Suriname">Suriname</option>
                        <option value="Svalbard and Jan Mayen" title="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                        <option value="Swaziland" title="Swaziland">Swaziland</option>
                        <option value="Sweden" title="Sweden">Sweden</option>
                        <option value="Switzerland" title="Switzerland">Switzerland</option>
                        <option value="Syrian Arab Republic" title="Syrian Arab Republic">Syrian Arab Republic</option>
                        <option value="Taiwan, Province of China" title="Taiwan, Province of China">Taiwan, Province of China</option>
                        <option value="Tajikistan" title="Tajikistan">Tajikistan</option>
                        <option value="Tanzania, United Republic of" title="Tanzania, United Republic of">Tanzania, United Republic of</option>
                        <option value="Thailand" title="Thailand">Thailand</option>
                        <option value="Timor-Leste" title="Timor-Leste">Timor-Leste</option>
                        <option value="Togo" title="Togo">Togo</option>
                        <option value="Tokelau" title="Tokelau">Tokelau</option>
                        <option value="Tonga" title="Tonga">Tonga</option>
                        <option value="Trinidad and Tobago" title="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option value="Tunisia" title="Tunisia">Tunisia</option>
                        <option value="Turkey" title="Turkey">Turkey</option>
                        <option value="Turkmenistan" title="Turkmenistan">Turkmenistan</option>
                        <option value="Turks and Caicos Islands" title="Turks and Caicos Islands">Turks and Caicos Islands</option>
                        <option value="Tuvalu" title="Tuvalu">Tuvalu</option>
                        <option value="Uganda" title="Uganda">Uganda</option>
                        <option value="Ukraine" title="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates" title="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom" title="United Kingdom">United Kingdom</option>
                        <option value="United States" title="United States">United States</option>
                        <option value="United States Minor Outlying Islands" title="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                        <option value="Uruguay" title="Uruguay">Uruguay</option>
                        <option value="Uzbekistan" title="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu" title="Vanuatu">Vanuatu</option>
                        <option value="Venezuela, Bolivarian Republic of" title="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option>
                        <option value="Viet Nam" title="Viet Nam">Viet Nam</option>
                        <option value="Virgin Islands, British" title="Virgin Islands, British">Virgin Islands, British</option>
                        <option value="Virgin Islands, U.S." title="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                        <option value="Wallis and Futuna" title="Wallis and Futuna">Wallis and Futuna</option>
                        <option value="Western Sahara" title="Western Sahara">Western Sahara</option>
                        <option value="Yemen" title="Yemen">Yemen</option>
                        <option value="Zambia" title="Zambia">Zambia</option>
                        <option value="Zimbabwe" title="Zimbabwe">Zimbabwe</option>
                    </select>

<?php echo getErrorLabel($validationResults, 'phone'); ?>
                    <input type="text" class="input-block-level" name="phone" id="phone" placeholder="Telefoonnummer" value="<?php echo getValue($validationResults, 'phone'); ?>">



                    <h2 class="registration-heading">Meld u aan</h2>
                    <h3>Bijkomende informatie</h3>

                    <?php echo getErrorLabel($validationResults, 'deliver'); ?>
                    <input type="text" class="input-block-level" name="deliver" id="deliver" placeholder="Leveradres">

<?php echo getErrorLabel($validationResults, 'payment'); ?>
                    <select class="input-block-level" name="payment" id="country" placeholder="Betalingsmethode" value="<?php echo getValue($validationResults, 'payment'); ?>"
                            style="margin-bottom:15px;">
                        <option>PayPal</option>
                        <option>Mastercard</option>
                        <option>Bancontact</option>
                        <option>VISA</option>
                        <option>American Express</option>
                    </select>
                    <h5>Terms of Services</h5>
                    <div style="height:300px;width:350px;border:1px solid #333;font:12px/22px Open Sans;overflow:auto;">
                        1. ACCEPTANCE OF TERMS

                        Welcome to Yahoo! UK&Ireland. Yahoo! provides its services to you, subject to the following Terms of Service ("TOS"). In these TOS "you" means the individual using the Yahoo! services and "Yahoo!" means Yahoo! UK Limited except where the relevant service is provided by another Yahoo! group company in which case references to Yahoo! shall, in respect of the services provided by such other Yahoo! group company, be references to that Yahoo! group company. If you are accessing the services logged in via a BTYahoo! ID, please see section 26 below. These TOS may may be updated by Yahoo! from time to time without notice to you. You should review the TOS periodically for changes at: http://info.yahoo.com/legal/uk/yahoo/utos/en-gb/.

                        In addition, when using particular Yahoo! services, you and Yahoo! shall be subject to any guidelines and rules applicable to such services which may be posted by Yahoo! from time to time. All such guides and rules are hereby incorporated by reference into the TOS. In most cases the guides and rules are specific to a particular part of the Services and will assist you in applying the TOS to that part, but to the extent of any inconsistency between the TOS and any guide or rule, the TOS will prevail. Yahoo! may also offer services from time to time that are governed by other terms of service. In such cases the other terms of service will be posted on the relevant service to which they apply. 

                        2. DESCRIPTION OF SERVICE 

                        Yahoo! currently provides users with access to a rich collection of on-line resources, including, various communications tools, online forums, shopping services, personalised content and branded programming through its network of properties (the "Services"). Unless explicitly stated otherwise, any new features that augment or enhance the current Services, including the release of new Yahoo! properties, shall be subject to the TOS. You acknowledge and agree that the Services are provided "AS IS" and that Yahoo! assumes no responsibility for the timeliness, deletion, mis-delivery or failure to store any user communications or personalisation settings. 

                        In order to use the Services, you must obtain access to the World Wide Web, either directly or through devices that access web-based content, and pay any service and/or telephony fees associated with such access. In addition, you must provide all equipment necessary to make such connection to the World Wide Web, including a computer and modem or other access device. Please be aware that Yahoo! has created certain areas on the Services that contain adult or mature content. You must be at least 18 years of age to access and view such areas.

                        3. YOUR REGISTRATION OBLIGATIONS AND SUPERVISING CHILDREN

                        In consideration of your use of the Services, you agree to: (a) provide true, accurate, current and complete information about yourself as prompted by the Services' registration form (such information being the "Registration Data") and (b) maintain and promptly update the Registration Data to keep it true, accurate, current and complete. If you provide any information that is untrue, inaccurate, not current or incomplete, or Yahoo! has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, Yahoo! has the right to suspend or terminate your account and refuse any and all current or future use of the Services (or any portion thereof). 

                        Yahoo! is concerned about the safety and privacy of all its users, particularly children. For this reason, parents who wish to allow their children access to the Services should assist them in setting up any relevant accounts and supervise their access to the Services. By allowing your child access to the Services, they will be able to access all of the Services including, email, message boards, groups, instant messages and chat (among others). Please remember that the Services are designed to appeal to a broad audience. Accordingly, as the legal guardian, it is your responsibility to determine whether any of the Services and/or Content (as defined in Section 6 below) are appropriate for your child.

                        4. YAHOO! PRIVACY POLICY 

                        You agree that Registration Data and certain other information about you will be processed in accordance with our Privacy Policy. For more information, please see our full privacy policy at http://info.yahoo.com/privacy/uk/yahoo/ 

                        5. MEMBER ACCOUNT, PASSWORD AND SECURITY 

                        You will receive a password and account designation upon completing the Services' registration process. You are responsible for maintaining the confidentiality of the password and account, and are fully responsible for all activities that occur under your password or account. You agree to (a) immediately notify Yahoo! of any unauthorised use of your password or account and any other breach of security, and (b) ensure that you exit from your account at the end of each session. Yahoo! cannot and will not be liable for any loss or damage arising from your failure to comply with this Section 5. 

                        6. MEMBER CONDUCT 

                        You acknowledge that all information, data, text, software, music, sound, photographs, graphics, video, messages and other materials ("Content"), whether publicly posted or privately transmitted, are the sole responsibility of the person from which such Content originated. This means that you, and not Yahoo! are entirely responsible for all Content that you upload, post, email or otherwise transmit via the Services. Yahoo! does not control the Content posted via the Services and, as such, does not guarantee the accuracy, integrity or quality of such Content. You acknowledge that by using the Services, you may be exposed to Content that is offensive, indecent or objectionable. Under no circumstances will Yahoo! be liable in any way for any Content, including, but not limited to, for any errors or omissions in any Content, or for any loss or damage of any kind incurred as a result of the use of any Content posted, emailed or otherwise transmitted via the Services. 

                        You agree to not use the Services to:

                        a. upload, post, email or otherwise transmit any Content that is unlawful, harmful, threatening, abusive, harassing, tortuous, defamatory, vulgar, obscene, libellous, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable; 

                        b. harm minors in any way; 

                        c. impersonate any person or entity, including, but not limited to, a Yahoo! official, forum leader, guide or host, or falsely state or otherwise misrepresent your affiliation with a person or entity; 

                        d. forge headers or otherwise manipulate identifiers in order to disguise the origin of any Content transmitted through the Services; 

                        e. upload, post, email or otherwise transmit any Content that you do not have a right to transmit under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements); 

                        f. upload, post, email or otherwise transmit any Content that infringes any patent, trademark, trade secret, copyright or other proprietary rights ("Rights") of any party; 

                        g. upload, post, email or otherwise transmit any unsolicited or unauthorised advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation, except in those areas that are designated for such purpose; 

                        h. upload, post, email or otherwise transmit any material that contains software viruses or any other computer code, files or programs designed to interrupt, damage, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; 

                        i. disrupt the normal flow of dialogue, cause a screen to "scroll" faster than other users of the Services are able to type, or otherwise act in a manner that negatively affects other users' ability to engage in real time exchanges; 

                        j. interfere with or disrupt the Services or servers or networks connected to the Services, or disobey any requirements, procedures, policies or regulations of networks connected to the Services; 

                        k. intentionally or unintentionally violate any applicable law or regulation including, but not limited to, regulations promulgated by any securities exchange; 

                        l. "stalk" or otherwise harass another; or 

                        m. extract, collect, process, combine or store personal data about other users. 

                        You acknowledge that Yahoo! has no obligation to monitor Content. Yahoo! and its designees shall have the right (but not the obligation) in their sole discretion to refuse, move or remove any Content that is available via the Services that violates the TOS or is otherwise objectionable. You agree that you must evaluate, and bear all risks associated with, the use of any Content, including any reliance on the accuracy, completeness, or usefulness of such Content. 

                        You agree that Yahoo! may access, preserve, and disclose your account information and Content: (a) to its affiliated companies worldwide for the purpose of providing the Content to you in an efficient manner; (b) for the purpose of properly administering your account in accordance with the standard operating procedures of Yahoo! or its affiliated companies; and (c) if required to do so by law or in the good faith belief that any such access, preservation or disclosure is reasonably necessary to: (i) comply with legal process; (ii) enforce the TOS; (iii) respond to claims that any Content violates the rights of third-parties; (iv) respond to your requests for customer service; or (v) protect the rights, property, or personal safety of Yahoo!, its users and the public. 

                        You acknowledge that the technical processing and transmission of the Services, including your Content, may involve transmissions over various networks and changes to conform and adapt to technical requirements of connecting networks or devices. Yahoo! reserves the right to terminate your access to some or all parts of its Services if you withdraw your consent in this paragraph at any time.

                        7. SPECIAL ADMONITIONS FOR INTERNATIONAL USE AND EXPORT AND IMPORT COMPLIANCE

                        Recognizing the global nature of the Internet, you agree to comply with all local rules regarding online conduct and acceptable Content. Use of the Yahoo! Services and transfer, posting and uploading of software, technology, and other technical data via the Yahoo! Services may be subject to the export and import laws of the United States and other countries. You agree to comply with all applicable export and import laws and regulations, including without limitation the Export Administration Regulations (see http://www.access.gpo.gov/bis/ear/ear_data.html and sanctions control programs of the United States (see http://www.treasury.gov/resource-center/sanctions/Programs/Pages/Programs.aspx. In particular, you represent and warrant that you: (a) are not a prohibited party identified on any government export exclusion lists (see http://www.bis.doc.gov/complianceandenforcement/liststocheck.htm or a member of a government of any other export-prohibited countries as identified in applicable export and import laws and regulations; (b) will not transfer software, technology, and other technical data via the Yahoo! Services to export-prohibited parties or countries; (c) will not use the Yahoo! Services for military, nuclear, missile, chemical or biological weaponry end uses in violation of U.S. export laws; and (d) will not transfer, upload, or post via the Yahoo! Services any software, technology or other technical data in violation of U.S. or other applicable export or import laws. 

                        8. PUBLIC CONTENT POSTED TO YAHOO!

                        (a) For purposes of the TOS, "publicly accessible areas" of the Services are those accessible by the general public. By way of example, a publicly accessible area of the Services would include public Yahoo! Groups, Yahoo! Answers and Yahoo! Message Boards, but would not include unlisted Yahoo! Groups or private Yahoo! communication services such as Yahoo! Mail or Yahoo! Messenger. You acknowledge that you are solely responsible for any personal data or information that you choose to disclose and make publicly accessible via the Services, and that under no circumstances will Yahoo! be liable in any way for the disclosure and public accessibility of such personal data or information. You acknowledge that any personal data or information (or any other Content) posted to publicly accessible areas may remain publicly accessible indefinitely.

                        (b) With respect to Content you elect to post for inclusion in publicly accessible areas of Yahoo! Groups or that consists of photos or other graphics you elect to post to any other publicly accessible area of the Services, you grant Yahoo! a world-wide, royalty free and non-exclusive licence to reproduce, modify, adapt and publish such Content on the Services solely for the purpose of displaying, distributing and promoting the specific Yahoo! Group to which such Content was submitted, or, in the case of photos or graphics, solely for the purpose for which such photo or graphic was submitted to the Services. This licence exists only for as long as you elect to continue to include such Content on the Services and shall be terminated at the time you delete such Content from the Services.

                        (c) With respect to all other Content you elect to post to other publicly accessible areas of the Services, you grant Yahoo! the royalty-free, perpetual, irrevocable, non-exclusive and fully sub-licensable right and licence to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform and display such Content (in whole or part) worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed. 

                        9. INDEMNITY 

                        You agree to indemnify and hold Yahoo! and its subsidiaries, affiliates, officers, agents, co-branders and other partners, and employees, harmless from any claim or demand, including reasonable attorneys' fees, made by any third party due to or arising out of Content you submit, post to or transmit through the Services, your use of the Services, your connection to the Services, your violation of the TOS, or your violation of any rights of another.

                        10. NO RESALE OF SERVICE 

                        You agree not to reproduce, duplicate, copy, sell, resell or exploit for any commercial purposes, any portion of the Services, use of the Services, or access to the Services. 

                        11. GENERAL PRACTICES REGARDING USE AND STORAGE 

                        You acknowledge that Yahoo! may establish general practices and limits concerning use of the Services, including with out limitation the maximum number of days that email messages, message board postings or other uploaded Content will be retained by the Services, the maximum number of email messages that may be sent from or received by an account on the Services, the maximum size of any email message that may be sent from or received by an account on the Services, the maximum disk space that will be allotted on Yahoo's servers on your behalf, and the maximum number of times (and the maximum duration for which) you may access the Services in a given period of time. You agree that Yahoo! has no responsibility or liability for the deletion or failure to store any messages and other communications or other Content maintained or transmitted by the Services. You acknowledge that Yahoo! reserves the right to log off accounts that are inactive for an extended period of time. You further acknowledge that Yahoo! reserves the right to change these general practices and limits at any time, in its sole discretion, with or without notice. 

                        12. MODIFICATIONS TO SERVICE 

                        Yahoo! reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Services (or any part thereof) with or without notice. You agree that Yahoo! shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Services. 

                        13. TERMINATION 

                        You agree that Yahoo! in its sole discretion, may terminate your password, account (or any part thereof) or use of the Services, and remove and discard any Content within the Services, for any reason, including, without limitation, for lack of use or if Yahoo! believes that you have violated or acted inconsistently with the letter or spirit of the TOS. Yahoo! may also in its sole discretion and at any time discontinue providing the Services, or any part thereof, with or without notice. You agree that any termination of your access to the Services under any provision of this TOS may be effected without prior notice, and acknowledge and agree that Yahoo! may immediately deactivate or delete your account and all related information and files in your account and/or bar any further access to such files or the Services. Further, you agree that Yahoo! shall not be liable to you or any third-party for any termination of your access to the Services. 

                        14. DEALINGS WITH ADVERTISERS

                        Your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Services, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such advertiser. To the fullest extent permitted by applicable law, you agree that Yahoo! shall not be responsible or liable for any loss or damage of any kind incurred as the result of any such dealings or as the result of the presence of such advertisers on the Service.

                        15. LINKS 

                        The Services may provide, or third parties may provide, links to other World Wide Web sites or resources. Because Yahoo! has no control over such sites and resources, you acknowledge and agree that Yahoo! is not responsible for the availability of such external sites or resources, and does not endorse and is not responsible or liable for any Content, advertising, products, or other materials on or available from such sites or resources. You further acknowledge and agree that Yahoo! shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such Content, goods or services available on or through any such site or resource. 

                        16. YAHOO'S PROPRIETARY RIGHTS 

                        You acknowledge and agree that the Services and any necessary software used in connection with the Services ("Software ") contain proprietary and confidential information that is protected by applicable intellectual property and other laws. You further acknowledge and agree that Content contained in sponsor advertisements or information presented to you through the Services or advertisers is protected by copyrights, trademarks, service marks, patents or other proprietary rights and laws. Except as expressly authorised by Yahoo! or advertisers, you agree not to modify, rent, lease, loan, sell, distribute or create derivative works based on the Services or the Software, in whole or in part. 

                        Yahoo! grants you a personal, non-transferable and non-exclusive right and licence to use the object code of its Software on a single computer; provided that you do not (and do not allow any third party to) copy, modify, create a derivative work of, reverse engineer, reverse assemble or otherwise attempt to discover any source code, sell, assign, sublicence, grant a security interest in or otherwise transfer any right in the Software. The foregoing is subject to applicable statute and other express law. You agree not to modify the Software in any manner or form, or to use modified versions of the Software, including (without limitation) for the purpose of obtaining unauthorised access to the Services. You agree not to access the Service by any means other than through the interface that is provided by Yahoo! for use in accessing the Services. 

                        17. DISCLAIMERS

                        YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT: 

                        A. YOUR USE OF THE SERVICE IS AT YOUR SOLE RISK. THE SERVICE IS PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, YAHOO! EXPRESSLY DISCLAIMS ALL WARRANTIES, CONDITIONS AND OTHER TERMS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO ANY IMPLIED TERM OF MERCHANTABILITY, SATISFACTORY Q UALITY, FITNESS FOR A PARTICULAR PURPOSE, AND ANY TERM AS TO THE PROVISION OF SERVICES TO A STANDARD OF REASONABLE CARE AND SKILL OR AS TO NON-INFRINGEMENT OF ANY INTELLECTUAL PROPERTY RIGHT. 

                        B. YAHOO! MAKES NO WARRANTY OR REPRESENTATION THAT (i) THE SERVICE WILL MEET YOUR REQUIREMENTS, (ii) THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, (iii) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICE WIL L BE ACCURATE OR RELIABLE, (iv) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTA INED BY YOU THROUGH THE SERVICE WILL MEET YOUR EXPECTATIONS, AND (V) ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED. 

                        C. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS DONE AT YOUR OWN DISCRETION AND RIS K AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.

                        D. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM YAHOO! OR THROUGH OR FROM THE SERVICE SHALL CREATE ANY WARRANTY OR OTHER OBLIGATION NOT EXPRESSLY STATED IN THE TOS. 

                        18. LIMITATION OF LIABILITY 

                        YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT YAHOO! SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF YAHOO! HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM: (i) THE USE OR THE INABILITY TO USE THE SERVICE; (ii) THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES RESULTING FROM ANY GOODS, DATA, INFORMATION OR SERVICES PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO THROUGH OR FROM THE SERVICE; (iii) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; (iv) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE SERVICE; OR (v) ANY OTHER MATTER RELATING TO THE SERVICE. 

                        YOU AGREE THAT REGARDLESS OF ANY STATUTE OR LAW TO THE CONTRARY, ANY CLAIM OR CAUSE OF ACTION ARISING OUT OF OR RELA TED TO USE OF THE SERVICE OF THESE TOS MUST BE FILED WITHIN ONE (1) YEAR AFTER SUCH CLAIM OR CAUSE OF ACTION AROSE OR BE FOREVER BARRED.

                        19. EXCLUSIONS AND LIMITATIONS 

                        SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS OF SECTIONS 17 AND 18 MAY NOT APPLY TO YOU.

                        IN PARTICULAR, NOTHING IN THESE TOS SHALL AFFECT THE STATUTORY RIGHTS OF ANY CONSUMER OR EXCLUDE OR RESTRICT ANY LIA BIITY FOR DEATH OR PERSONAL INJURY ARISING FROM THE NEGLIGENCE OR FRAUD OF YAHOO!.

                        20. SPECIAL ADMONITION FOR SERVICES RELATING TO FINANCIAL MATTERS 

                        If you intend to create or join any service, receive or request any news, messages, alerts or other information from the Services concerning companies, stock quotes, investments or securities, please read the above Sections 17 and 18 again. They go doubly for you. In addition, for this type of information particularly, the phrase "Let the investor beware" is apt. The Services are provided for informational purposes only, and no Content included in the Services is intended for trading or investing purposes. Yahoo! shall not be responsible or liable for the accuracy, usefulness or availability of any information transmitted via the Services, and shall not be responsible or liable for any trading or investment decisions made based on such information. 

                        21. NOTICE 

                        Notices to you may be made via either email or regular mail. The Services may also provide notices of changes to the TOS or other matters by displaying notices or links to notices to you generally on the Services. 

                        22. TRADEMARK INFORMATION 

                        Yahoo!, the Yahoo! logo, Yahoo! in Chinese Characters, Yahooligans!, the Yahooligans! logo, YAHOO! KIDS, Yahoo! Kids logo, the "Jumpin Y Guy" logo, Do you Yahoo!?, Y!, Y! stylised, My Yahoo!, Eyeballs design, Homework Answers, The Scoop, Get Local, the Worlds Favorite Internet Guide, the Sunglasses design, e Café Yahoo!, Viaweb, Shop Find, EZSpree, EZWheels, I Shops, Permission Marketing, Yoyodyne, Get Rich Click, and Geocities trademarks and service marks, and other Yahoo! logos and product and service names are trademarks of Yahoo! Inc. (the "Yahoo! Marks"). Without Yahoo's prior permission, you agree not to display or use in any manner, the Yahoo! Marks.

                        23. COPYRIGHTS and COPYRIGHT AGENTS 

                        Yahoo! respects the intellectual property of others, and we ask our users to do the same. If you believe that any of your intellectual property rights have been infringed on the Services, please go to our copyright policy to report the problem.

                        24. GENERAL INFORMATION

                        These TOS (including the guides and rules referred to herein) constitute the entire agreement between you and Yahoo! and govern your use of the Services, superseding any prior agreements between you and Yahoo! You also may be subject to additional terms and conditions that may apply when you use affiliate services, third-party content or third-party software. The TOS and the relationship between you and Yahoo! shall be governed by the laws of England. You and Yahoo! agree to submit to the exclusive jurisdiction of the English courts. Any failure by Yahoo! to exercise or enforce any right or provision of the TOS shall not constitute a waiver of such right or provision. If any provision of the TOS is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavour to give effect to the parties' intentions as reflected in the provision, and the other provisions of the TOS remain in full force and effect. 

                        Neither you nor Yahoo! may assign or transfer any rights or obligations under these TOS without the prior written consent of the other party, except that Yahoo! shall be entitled to assign or transfer any or all of its rights and obligations (without your prior consent) to any of its affiliated companies. 

                        The section titles in the TOS are for convenience only and have no legal or contractual effect. No Right of Survivorship and Non-Transferability. You agree that your Yahoo! account is non-transferable and any rights to your Yahoo! ID or contents within your account terminate upon your death. Any free account that has not been used for a certain period of time may be terminated and all contents therein permanently deleted in line with Yahoo!'s policy. 

                        25. VIOLATIONS 

                        Please report any violations of the TOS to Yahoo! Customer Care. 

                        26. BT YAHOO! USERS

                        If you are accessing the Services logged in via a BTYahoo! ID, then the terms of this TOS apply to you with respect to the Services but BT Telecommunications Plc remains responsible for providing all services within the BTYahoo! Service and handling your personal data. Click here for more information.
                    </div>
                    <label class="checkbox" style="">
                        <input type="checkbox" name="ToS" value="accept"> Accepteren
                    </label>
                    <label class="checkbox" style="clear:both;margin:10px 0 20px 0;">
                        <input type="checkbox" value="remember-me"> Schrijf mij in voor de nieuwsbrief
                    </label>
                    <div id="buttons">
                        <button class="btn btn-large btn-primary" name="register" id="register" type="submit">Registreer</button>
                        <a href="index.php" class="btn btn-large btn-primary" id="keertrug">Keer terug</a>
                    </div>
                </form>
            </div>
        </div>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap-transition.js"></script>
        <script src="assets/js/bootstrap-alert.js"></script>
        <script src="assets/js/bootstrap-modal.js"></script>
        <script src="assets/js/bootstrap-dropdown.js"></script>
        <script src="assets/js/bootstrap-scrollspy.js"></script>
        <script src="assets/js/bootstrap-tab.js"></script>
        <script src="assets/js/bootstrap-tooltip.js"></script>
        <script src="assets/js/bootstrap-popover.js"></script>
        <script src="assets/js/bootstrap-button.js"></script>
        <script src="assets/js/bootstrap-collapse.js"></script>
        <script src="assets/js/bootstrap-carousel.js"></script>
        <script src="assets/js/bootstrap-typeahead.js"></script>

    </body>
</html>
