<?php

function validateEmail($email)
{
    $result = array();
    $error = null;

    if ($email != '') {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Please enter a valid email address:';
        }
    } else {
        $error = 'Please enter your email address:';
    }
    
    $result['value'] = $email;
    $result['error'] = $error;
    
    return $result;
}
function validateCheckbox($tos)
{
    $result = array();
    $error = null;

    if ($tos != '') {
        $tos = filter_var($tos, FILTER_SANITIZE_BOOLEAN);

        if (!filter_var($tos, FILTER_VALIDATE_BOOLEAN)) {
            $error = 'Checkeuh da box:';
        }
    } else {
        $error = 'You must agree to the Terms & Services:';
    }
    
    $result['value'] = $tos;
    $result['error'] = $error;
    
    return $result;
}

function validatePassword($password, $passwordConfirmation=false)
{
    $result = array();
    $error = null;

    if ($password != '') {
        $password = filter_var($password, FILTER_SANITIZE_STRING);

        if ($password == '') {
            $error = 'Please enter a valid password:';
        }
    } else {
        // Begin met de default waarde
        $error = 'Please enter your password:';

        if ($passwordConfirmation == true) {
            // Wijs de uitzondering toe.
            $error = 'Please enter your password for confirmation:';
        }
    }

    $result['value'] = $password;
    $result['error'] = $error;
    
    return $result;
}

function validateString($string, $label)
{
    $result = array();
    $error = null;

    if ($string != '') {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
    } else {
        $error = "Please enter $label:";
    }

    $result['value'] = $string;
    $result['error'] = $error;
    
    return $result;
}

?>
